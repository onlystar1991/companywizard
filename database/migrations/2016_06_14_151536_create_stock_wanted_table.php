<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockWantedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_wanted', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('contact_person'); // Users table id
            $table->integer('matching_search_notification');
            $table->string('listing_date');
            $table->boolean('only_advertise_internally');
            $table->string('advertise_until');
            $table->integer('quantity');
            $table->integer('species_id'); // Live Stock Types Table ID
            $table->integer('type_id'); // ListType  Table ID
            $table->integer('category_id'); // Category Table ID
            $table->integer('breed_id'); //  Breed Table ID
            $table->string('description');
            $table->integer('location');
            $table->string('delivered_location');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stock_wanted');
    }
}
