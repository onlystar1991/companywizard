<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesCalendarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_calendars', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id');
            $table->string('contact_persons');
            $table->integer('listed_species');
            $table->integer('type_of_sales');
            $table->integer('state_id');
            $table->string('date_of_sale');
            $table->integer('frequency');
            $table->string('recur_until');
            $table->string('sale_title');
            $table->integer('auction_type');
            $table->string('location_of_sale');
            $table->integer('exclusion_dates');

            $table->string('elect_cat_avaliable');
            $table->boolean('is_elders_xtra_sale');
            $table->boolean('hard_copy_cat_available');
            $table->string('sales_details');
            $table->string('link');
            $table->boolean('photo_available');
            $table->boolean('video_available');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales_calendars');
    }
}