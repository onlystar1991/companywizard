@extends('layouts.stock')

{{-- Web site Title --}}
@section('title') {!! "Test" !!} :: @parent @endsection

@section('styles')
<link href="{{ asset('assets/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
<style>
    #content td {
        border: 1px solid #d1d1d1;
        vertical-align: middle;
    }
    #content th, td {
        padding: 0.7em !important;
    }
    #stock_id {
        width: 18% !important;
        margin-top: 5px;
        min-width: 150px;
    }
    #search_form {
        border: 1px lightgray solid;
        padding: 10px;
    }
</style>

@endsection

{{-- Content --}}
@section('content')
    <h1>
        Hello Stock for Sale Tabs
    </h1>
@endsection
{{-- Scripts --}}
@section('scripts')
@endsection
