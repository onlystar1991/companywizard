@extends('layouts.stock')

{{-- Web site Title --}}
@section('title') {!! "Test" !!} :: @parent @endsection


@section('styles')
<link href="{{ asset('assets/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
<style>
    .content-wrapper td {
        border: 1px solid #d1d1d1;
        vertical-align: middle;
    }
    .content-wrapper th, td {
        padding: 0.7em !important;
    }
    #stock_id {
        width: 18% !important;
        margin-top: 5px;
        min-width: 150px;
    }
    #search_form {
        border: 1px lightgray solid;
        padding: 10px;
    }
</style>
@endsection
{{-- Content --}}
@section('content')
    <a href="#" class="btn btn-primary">New</a>
    
    <form class="form form-horizontal" style="margin-top: 10px;">
    	<table>
        	<tr>
                <td colspan="4">
                    ID number<br>
                    <input type="text" name="id" id="stock_id" class="form-control">
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    OR
                </td>
            </tr>
            <tr>
                <td>
                    Listed species
                    <select name="list_type_id" id="species_type" class="form-control select2-selection__rendered" >
	                    <option value="">All</option>
	                    @foreach ($stock_types as $stock_type)
	                    	<option value="{!! $stock_type->id !!}">{!! $stock_type->name !!}</option>
			            @endforeach
					</select>
                </td>
                <td>
                    Listing type
                    <select name="SearchListingTypeId" id="SearchListingTypeId" class="form-control">
                        <option value="">All</option>
						@foreach ($list_types as $list_type)
	                    	<option value="{!! $list_type->id !!}">{!! $list_type->name !!}</option>
			            @endforeach
					</select>
                </td>
                <td>
                    Category
                    <select name="SearchCategory" id="SearchCategory" class="form-control">
                    	<option value="">All</option>
                    	@foreach ($categories as $category)
	                    	<option value="{!! $category->id !!}">{!! $category->name !!}</option>
			            @endforeach
                    </select>
                </td>
                <td>
                    Breed
                    <select name="SearchBreed" id="SearchBreed" class="form-control">
                    	<option value="">All</option>
                    	@foreach ($breeds as $breed)
	                    	<option value="{!! $breed->id !!}">
	                    		{!! $breed->breed_name !!}
	                    	</option>
			            @endforeach
					</select>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    Contact person
                    <select name="SearchContactPersonId" id="SearchContactPersonId" class="form-control">
                        <option value="">All</option>
                        @foreach ($contact_persons as $contact_person)
                            <option value="{!! $contact_person->id !!}">
                                {!! $contact_person->username . ' : ' . $contact_person->name . ', ' . $contact_person->email !!}
                            </option>
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    Contact branch
                    <select name="SearchContactBranchId" id="SearchContactBranchId" class="form-control">
                        <option value="">All</option>
                        @foreach ($branches as $branch)
                            <option value="{!! $branch->id !!}">
                                {!! $branch->name !!}
                            </option>
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Date from:
                    <div id="datetimepicker1" class="input-group date">
                        <input type="text" value="" name="SearchDateFrom" id="SearchDateFrom" class="hasDatepicker form-control">
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                </td>
                <td colspan="2">
                    Date to:
                    <div id="datetimepicker2" class="input-group date">
                        <input type="text" value="" name="SearchDateTo" id="SearchDateTo" class="hasDatepicker form-control">
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                </td>
            </tr>
            
            <tr>
                <td colspan="4">
                    State(s):<br>
                    <div class="checkbox c-checkbox">
                        <label style="margin: 10px;">
                            <input type="checkbox" checked="checked" value="13" name="SearchState" data="Am" id="SearchState">
                            <span class="fa fa-check"></span>Am
                        </label>
                    
                        <label style="margin: 10px;">
                            <input type="checkbox" checked="checked" value="1" name="SearchState" data="ACT" id="SearchState">
                            <span class="fa fa-check"></span>ACT
                        </label>
                        
                        <label style="margin: 10px;">
                            <input type="checkbox" checked="checked" value="2" name="SearchState" data="NSW" id="SearchState">
                            <span class="fa fa-check"></span>NSW
                        </label>
                        
                        <label style="margin: 10px;">
                            <input type="checkbox" checked="checked" value="5" name="SearchState" data="NT" id="SearchState">
                            <span class="fa fa-check"></span>NT
                        </label>
                        
                        <label style="margin: 10px;">
                            <input type="checkbox" checked="checked" value="6" name="SearchState" data="QLD" id="SearchState">
                            <span class="fa fa-check"></span>QLD
                        </label>
                        
                        <label style="margin: 10px;">
                            <input type="checkbox" checked="checked" value="12" name="SearchState" data="Ro" id="SearchState">
                            <span class="fa fa-check"></span>Ro
                        </label>
                        
                        <label style="margin: 10px;">
                            <input type="checkbox" checked="checked" value="7" name="SearchState" data="SA" id="SearchState">
                            <span class="fa fa-check"></span>SA
                        </label>
                        
                        <label style="margin: 10px;">
                            <input type="checkbox" checked="checked" value="8" name="SearchState" data="TAS" id="SearchState">
                            <span class="fa fa-check"></span>TAS
                        </label>
                        
                        <label style="margin: 10px;">
                            <input type="checkbox" checked="checked" value="9" name="SearchState" data="VIC" id="SearchState">
                            <span class="fa fa-check"></span>VIC
                        </label>
                        
                        <label style="margin: 10px;">
                            <input type="checkbox" checked="checked" value="11" name="SearchState" data="WA" id="SearchState">
                            <span class="fa fa-check"></span>WA
                        </label>
                    </div>
                </td>
            </tr>
            <tr class="screen-only">
                <td colspan="4">
                    <input type="submit" value=" View" class="btn btn-primary" name="btnSearch" id="btnSearch">
                </td>
            </tr>
        </table>
    </form>
@endsection
{{-- Scripts --}}
@section('scripts')
    
    
    <script src="{{ asset('assets/bootstrap-filestyle/src/bootstrap-filestyle.js') }}"></script>
    <script src="{{ asset('assets/chosen_v1.2.0/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('assets/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js') }}"></script>
    <script src="{{ asset('assets/jquery.inputmask/dist/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('assets/bootstrap-wysiwyg/bootstrap-wysiwyg.js') }}"></script>
    <script src="{{ asset('assets/bootstrap-wysiwyg/external/jquery.hotkeys.js') }}"></script>
    <script src="{{ asset('assets/moment/min/moment-with-locales.min.js') }}"></script>

    <script src="{{ asset('assets/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/demo/demo-forms.js') }}"></script>
@endsection

