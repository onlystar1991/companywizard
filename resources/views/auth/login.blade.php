@extends('layouts.auth')

{{-- Web site Title --}}
@section('title') {!!  trans('site/user.login') !!} :: @parent @endsection


{{-- Style --}}
@section('styles')
<link href="{{ asset('css/login/app.css') }}" rel="stylesheet">
@endsection


{{-- Content --}}
@section('content')
    <!-- START panel-->
    <div class="panel panel-dark panel-flat">
        <div class="panel-heading text-center">
            <a href="#">
                <img src="{{ asset('img/login/logo.png') }}" alt="" class="block-center img-rounded">
            </a>
        </div>
        
        <div class="panel-body">
            <p class="text-center pv">{!! trans('site/user.login_to_account') !!}.</p>
            {!! Form::open(array('url' => url('auth/login'),'class' => 'mb-lg', 'data-parsley-validate' => '', 'novalidate' => '',  'role' => 'form', 'method' => 'post', 'files'=> true)) !!}
                <div class="form-group has-feedback">
                    {!! Form::text('email', null, array('class' => 'form-control ' . ($errors->has('email') ? 'parsley-error' : ''), 'autocomplete' => 'off', 'type' => 'email', 'placeholder' => 'Enter Email')) !!}
                    <span class="fa fa-envelope form-control-feedback text-muted"></span>
                    <ul id="parsley-id-4" class="parsley-errors-list filled">
                        <li class="parsley-required">{{ $errors->first('email', ':message') }}</li>
                    </ul>
                </div>
                
                <div class="form-group has-feedback">
                    {!! Form::password('password', array('class' => 'form-control '. ($errors->has('password') ? 'parsley-error' : ''), 'placeholder' => 'Password', 'type' => 'password')) !!}
                    <span class="fa fa-lock form-control-feedback text-muted"></span>
                    <ul id="parsley-id-4" class="parsley-errors-list filled">
                        <li class="parsley-required">{{ $errors->first('password', ':message') }}</li>
                    </ul>
                </div>

                <div class="clearfix">
                    <div class="checkbox c-checkbox pull-left mt0">
                        <label>
                            <input type="checkbox" value="" name="remember">
                            <span class="fa fa-check"></span>Remember Me
                        </label>
                    </div>
                    <div class="pull-right">
                        <a href="{{ url('/password/email') }}">Forgot Your Password?</a>
                    </div>
                </div>

                <button type="submit" class="btn btn-block btn-primary mt-lg">Login</button>
            {!! Form::close() !!}

            <p class="pt-lg text-center">Need to Signup?</p>
            <a href="{{ url('/password/email') }}" class="btn btn-block btn-default">Register Now</a>
        </div>
    </div>
    <!-- END panel-->
@endsection

@section('scripts')
<script src="{{ asset('js/login/app.js') }}"></script>
@endsection
