<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@section('title') Laravel 5 Sample Site </title>@show
    @section('meta_keywords')
        <meta name="keywords" content="your, awesome, keywords, here"/>
    @show @section('meta_author')
        <meta name="author" content="Jon Doe"/>
    @show @section('meta_description')
        <meta name="description" content="Lorem ipsum dolor sit amet, nihil fabulas et sea, nam posse menandri scripserit no, mei."/>
    @show
		<link href="{{ asset('assets/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/animate.css/animate.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/whirl/dist/whirl.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">

    @yield('styles')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="{!! asset('assets/site/ico/favicon.ico')  !!} ">
</head>
<body>
@include('partials.nav')

<div class="container">
@yield('content')
</div>
@include('partials.footer')

<script src="{{ asset('assets/modernizr/modernizr.custom.js') }}"></script>
<script src="{{ asset('assets/jquery/dist/jquery.js') }}"></script>
<script src="{{ asset('assets/jQuery-Storage-API/jquery.storageapi.js') }}"></script>
<script src="{{ asset('assets/jquery.easing/js/jquery.easing.js') }}"></script>
<script src="{{ asset('assets/animo.js/animo.js') }}"></script>
<script src="{{ asset('assets/jquery-localize-i18n/dist/jquery.localize.js') }}"></script>
<!-- Scripts -->
@yield('scripts')

</body>
</html>
