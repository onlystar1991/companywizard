<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@section('title') Laravel 5 Sample Site </title>@show
        @section('meta_keywords')
            <meta name="keywords" content="your, awesome, keywords, here"/>
        @show @section('meta_author')
            <meta name="author" content="Jon Doe"/>
        @show @section('meta_description')
            <meta name="description" content="Lorem ipsum dolor sit amet, nihil fabulas et sea, nam posse menandri scripserit no, mei."/>
        @show
    		<link href="{{ asset('assets/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
            <link href="{{ asset('assets/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
            <link href="{{ asset('assets/animate.css/animate.min.css') }}" rel="stylesheet">
            
            <link href="{{ asset('assets/whirl/dist/whirl.css') }}" rel="stylesheet">
            <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" id="bscss">
            <link href="{{ asset('css/app.css') }}" rel="stylesheet" id="maincss">
        @yield('styles')
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><script/>
        <![endif]-->

        <link rel="shortcut icon" href="{!! asset('assets/site/ico/favicon.ico')  !!} ">
    </head>
<body>
    <div class="wrapper">
        <header class="topnavbar-wrapper">
            <nav role="navigation" class="navbar topnavbar">
            <!-- START navbar header-->
                <div class="navbar-header">
                    <a href="#/" class="navbar-brand">
                        <div class="brand-logo">
                            <img src="{{ asset('img/elders-logo1.png')}}" alt="App Logo" class="img-responsive">
                        </div>
                        <div class="brand-logo-collapsed">
                            <img src="{{ asset('img/elders-logo1.png')}}" alt="App Logo" class="img-responsive">
                        </div>
                    </a>
                </div>
                <!-- END navbar header-->
                <!-- START Nav wrapper-->
                <div class="nav-wrapper">
                    <!-- START Left navbar-->
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#" data-trigger-resize="" data-toggle-state="aside-collapsed" class="hidden-xs">
                                <em class="fa fa-navicon"></em>
                            </a>
                            <!-- Button to show/hide the sidebar on mobile. Visible on mobile only.-->
                            <a href="#" data-toggle-state="aside-toggled" data-no-persist="true" class="visible-xs sidebar-toggle">
                                <em class="fa fa-navicon"></em>
                            </a>
                        </li>
                    </ul>
                    <!-- END Left navbar-->
                    <!-- START Right Navbar-->
                    <ul class="nav navbar-nav navbar-right">
                    <!-- Search icon-->
                        <li>
                            <a href="{{ url('auth/logout') }}" data-search-open="">
                                <em class="icon-logout"></em>
                            </a>
                        </li>
                    </ul>
                    <!-- END Right Navbar-->
                </div>
                <!-- END Nav wrapper-->
            </nav>
            <!-- END Top Navbar-->
        </header>

        <aside class="aside">
            <div class="aside-inner">
                <nav data-sidebar-anyclick-close="" class="sidebar">
                    <ul class="nav">
                        <li class="{!! $active_tab == 0 ? 'active' : '' !!}">
                            <a href="{!! URL::to('/') !!}/livestockAdmin" title="Dashboard">
                                <em class="icon-speedometer"></em>
                                <span data-localize="sidebar.nav.DASHBOARD">Stock wanted</span>
                            </a>
                        </li>
                        <li class="{!! $active_tab == 1 ? 'active' : '' !!}">
                            <a href="{!! URL::to('/') !!}/stockforsale" title="Widgets">
                                <em class="icon-grid"></em>
                                <span data-localize="sidebar.nav.WIDGETS">Stock for sale</span>
                            </a>
                        </li>
                        <li class="{!! $active_tab == 2 ? 'active' : '' !!}">
                            <a href="#" title="Layouts">
                                <em class="icon-layers"></em>
                                <span>Clearing sales</span>
                            </a>
                        </li>

                        <li class="{!! $active_tab == 3 ? 'active' : '' !!}">
                            <a href="#" title="Elements" data-toggle="collapse">
                                <em class="icon-chemistry"></em>
                                <span data-localize="sidebar.nav.element.ELEMENTS">Sales calendar</span>
                            </a>
                        </li>

                        <li class="{!! $active_tab == 4 ? 'active' : '' !!}">
                            <a href="#" title="Forms" data-toggle="collapse">
                                <em class="icon-note"></em>
                                <span data-localize="sidebar.nav.form.FORM">States</span>
                            </a>
                        </li>
                        <li class="{!! $active_tab == 5 ? 'active' : '' !!}">
                            <a href="#" title="Charts" data-toggle="collapse">
                                <em class="icon-graph"></em>
                                <span data-localize="sidebar.nav.chart.CHART">Branches</span>
                            </a>
                        </li>
                        <li class="{!! $active_tab == 6 ? 'active' : '' !!}">
                            <a href="#" title="Charts" data-toggle="collapse">
                                <em class="icon-graph"></em>
                                <span data-localize="sidebar.nav.chart.CHART">Staff</span>
                            </a>
                        </li>
                        <li class="{!! $active_tab == 7 ? 'active' : '' !!}">
                            <a href="#charts" title="Charts" data-toggle="collapse">
                                <em class="icon-graph"></em>
                                <span data-localize="sidebar.nav.chart.CHART">Saved searches</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        
        <section>
            <div class="content-wrapper">
                @yield('content')
            </div>
        </section>
    </div>

        @include('partials.footer')
        <script src="{{ asset('assets/modernizr/modernizr.custom.js') }}"></script>
        <script src="{{ asset('assets/matchMedia/matchMedia.js') }}"></script>
        <script src="{{ asset('assets/jquery/dist/jquery.js') }}"></script>
        <script src="{{ asset('assets/bootstrap/dist/js/bootstrap.js') }}"></script>
        <script src="{{ asset('assets/jQuery-Storage-API/jquery.storageapi.js') }}"></script>
        <script src="{{ asset('assets/jquery.easing/js/jquery.easing.js') }}"></script>
        <script src="{{ asset('assets/animo.js/animo.js') }}"></script>
        <script src="{{ asset('assets/slimScroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('assets/screenfull/dist/screenfull.js') }}"></script>
        <script src="{{ asset('assets/jquery-localize-i18n/dist/jquery.localize.js') }}"></script>
        @yield('scripts')
        
        <script src="{{ asset('js/demo/demo-rtl.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>
        
</body>
</html>