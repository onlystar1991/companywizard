<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@section('title') Laravel 5 Sample Site @show</title>
    @section('meta_keywords')
        <meta name="keywords" content="your, awesome, keywords, here"/>
    @show @section('meta_author')
        <meta name="author" content="Jon Doe"/>
    @show @section('meta_description')
        <meta name="description" content="Lorem ipsum dolor sit amet, nihil fabulas et sea, nam posse menandri scripserit no, mei."/>
    @show
		<link href="{{ asset('assets/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">

    @yield('styles')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="{!! asset('assets/site/ico/favicon.ico')  !!} ">
</head>
<body>
    <div class="wrapper">
        <div class="block-center mt-xl wd-xl">
            @yield('content')
            <div class="p-lg text-center">
                <span>&copy;</span>
                <span>2016</span>
                <span>-</span>
                <span>Angle</span>
                <br>
                <span>Bootstrap Admin Template</span>
            </div>
        </div>
    </div>

    @include('partials.footer')

    <script src="{{ asset('assets/modernizr/modernizr.custom.js') }}"></script>
    <script src="{{ asset('assets/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('assets/bootstrap/dist/js/bootstrap.js') }}"></script>
    <script src="{{ asset('assets/jQuery-Storage-API/jquery.storageapi.js') }}"></script>
    <script src="{{ asset('assets/parsleyjs/dist/parsley.min.js') }}"></script>

    <!-- Scripts -->
    @yield('scripts')

</body>
</html>
