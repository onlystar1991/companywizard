<?php

namespace App\Http\Controllers;

use App\Article;
use App\PhotoAlbum;
use Illuminate\Support\Facades\Auth;
use DB;

class HomeController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::guest()) {
			return redirect('/auth/login');
		}

		if (Auth::user()->admin == 1) {
			return redirect('/admin/dashboard');
		} else {
			return view('pages.home');
		}
	}
}